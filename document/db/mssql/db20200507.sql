
GO
/****** Object:  Table [dbo].[t_refund_order]    Script Date: 05/07/2020 21:52:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[t_refund_order](
	[refund_order_id] [varchar](30) NOT NULL,
	[pay_order_id] [varchar](30) NOT NULL,
	[channel_pay_order_no] [varchar](64) NULL,
	[mch_id] [varchar](30) NOT NULL,
	[mch_refund_no] [varchar](30) NOT NULL,
	[channel_id] [varchar](24) NOT NULL,
	[pay_amount] [bigint] NOT NULL,
	[refund_amount] [bigint] NOT NULL,
	[currency] [varchar](3) NOT NULL,
	[status] [int] NOT NULL,
	[result] [int] NOT NULL,
	[client_ip] [varchar](32) NULL,
	[device] [varchar](64) NULL,
	[remark_info] [varchar](256) NULL,
	[channel_user] [varchar](32) NULL,
	[user_name] [varchar](24) NULL,
	[channel_mch_id] [varchar](32) NOT NULL,
	[channel_order_no] [varchar](32) NULL,
	[channel_err_code] [varchar](128) NULL,
	[channel_err_msg] [varchar](128) NULL,
	[extra] [varchar](512) NULL,
	[notify_url] [varchar](128) NOT NULL,
	[param1] [varchar](64) NULL,
	[param2] [varchar](64) NULL,
	[expire_time] [datetime] NULL,
	[refund_succ_time] [datetime] NULL,
	[create_by] [varchar](64) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](64) NULL,
	[update_time] [datetime] NULL,
	[remark] [varchar](500) NULL,
 CONSTRAINT [PK_t_refund_order] PRIMARY KEY CLUSTERED 
(
	[refund_order_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'退款单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'refund_order_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'pay_order_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'渠道支付单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'channel_pay_order_no'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商户ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'mch_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商户退款单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'mch_refund_no'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'渠道ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'channel_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'pay_amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'退款金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'refund_amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'币种' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'currency'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'结果' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'result'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'客户端IP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'client_ip'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设备' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'device'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'remark_info'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'渠道用户' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'channel_user'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'user_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'渠道商户号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'channel_mch_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'渠道订单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'channel_order_no'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'渠道错误码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'channel_err_code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'渠道错误信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'channel_err_msg'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'附加' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'extra'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'通知地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'notify_url'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参数1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'param1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参数2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'param2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'过期时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'expire_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'refund_succ_time'
GO
/****** Object:  Table [dbo].[t_pay_order]    Script Date: 05/07/2020 21:52:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[t_pay_order](
	[pay_order_id] [varchar](30) NOT NULL,
	[mch_id] [varchar](30) NOT NULL,
	[mch_order_no] [varchar](30) NOT NULL,
	[channel_id] [varchar](24) NOT NULL,
	[amount] [bigint] NOT NULL,
	[currency] [varchar](3) NOT NULL,
	[status] [int] NOT NULL,
	[client_ip] [varchar](32) NULL,
	[device] [varchar](64) NULL,
	[subject] [varchar](64) NOT NULL,
	[body] [varchar](256) NOT NULL,
	[extra] [varchar](512) NULL,
	[scene] [varchar](100) NULL,
	[auth_code] [varchar](100) NULL,
	[channel_mch_id] [varchar](32) NOT NULL,
	[channel_order_no] [varchar](64) NULL,
	[err_code] [varchar](64) NULL,
	[err_msg] [varchar](128) NULL,
	[param1] [varchar](256) NULL,
	[param2] [varchar](256) NULL,
	[notify_url] [varchar](128) NOT NULL,
	[notify_count] [int] NOT NULL,
	[last_notify_time] [bigint] NULL,
	[expire_time] [bigint] NULL,
	[pay_succ_time] [bigint] NULL,
	[create_by] [varchar](64) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](64) NULL,
	[update_time] [datetime] NULL,
	[remark] [varchar](500) NULL,
 CONSTRAINT [PK__t_pay_or__E0EF5B9F1367E606] PRIMARY KEY CLUSTERED 
(
	[pay_order_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'pay_order_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商户ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'mch_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商户订单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'mch_order_no'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'渠道ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'channel_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'币种' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'currency'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'客户端IP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'client_ip'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设备' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'device'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主题' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'subject'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'内容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'body'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'附加' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'extra'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商户渠道ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'channel_mch_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'渠道订单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'channel_order_no'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'错误码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'err_code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'错误信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'err_msg'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参数1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'param1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参数2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'param2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'通知地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'notify_url'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'通知次数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'notify_count'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最后通知时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'last_notify_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'过期时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'expire_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_order', @level2type=N'COLUMN',@level2name=N'pay_succ_time'
GO
/****** Object:  Table [dbo].[t_pay_channel]    Script Date: 05/07/2020 21:52:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[t_pay_channel](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[channel_id] [varchar](24) NOT NULL,
	[channel_name] [varchar](30) NOT NULL,
	[channel_mch_id] [varchar](32) NOT NULL,
	[mch_id] [varchar](30) NOT NULL,
	[status] [int] NOT NULL,
	[param] [varchar](4096) NOT NULL,
	[create_by] [varchar](64) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](64) NULL,
	[update_time] [datetime] NULL,
	[remark] [varchar](500) NULL,
 CONSTRAINT [PK__t_pay_ch__3214EC07060DEAE8] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_channel', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'渠道id	' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_channel', @level2type=N'COLUMN',@level2name=N'channel_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'渠道名称	' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_channel', @level2type=N'COLUMN',@level2name=N'channel_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'渠道商户id
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_channel', @level2type=N'COLUMN',@level2name=N'channel_mch_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商户id
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_channel', @level2type=N'COLUMN',@level2name=N'mch_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_channel', @level2type=N'COLUMN',@level2name=N'status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_channel', @level2type=N'COLUMN',@level2name=N'param'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_pay_channel', @level2type=N'COLUMN',@level2name=N'remark'
GO
/****** Object:  Table [dbo].[t_mch_notify]    Script Date: 05/07/2020 21:52:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[t_mch_notify](
	[order_id] [varchar](30) NOT NULL,
	[mch_id] [varchar](30) NOT NULL,
	[mch_order_no] [varchar](30) NOT NULL,
	[order_type] [varchar](8) NOT NULL,
	[notify_url] [varchar](2048) NOT NULL,
	[notify_count] [int] NOT NULL,
	[result] [varchar](2048) NULL,
	[status] [int] NOT NULL,
	[last_notify_time] [datetime] NULL,
	[create_by] [varchar](64) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](64) NULL,
	[update_time] [datetime] NULL,
	[remark] [varchar](500) NULL,
 CONSTRAINT [PK__t_mch_no__C3905BCF5CD6CB2B] PRIMARY KEY CLUSTERED 
(
	[order_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单号	' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_mch_notify', @level2type=N'COLUMN',@level2name=N'order_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商户ID	' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_mch_notify', @level2type=N'COLUMN',@level2name=N'mch_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商户订单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_mch_notify', @level2type=N'COLUMN',@level2name=N'mch_order_no'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_mch_notify', @level2type=N'COLUMN',@level2name=N'order_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'通知路径' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_mch_notify', @level2type=N'COLUMN',@level2name=N'notify_url'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'通知次数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_mch_notify', @level2type=N'COLUMN',@level2name=N'notify_count'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'通知结果' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_mch_notify', @level2type=N'COLUMN',@level2name=N'result'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_mch_notify', @level2type=N'COLUMN',@level2name=N'status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最后通知时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_mch_notify', @level2type=N'COLUMN',@level2name=N'last_notify_time'
GO
/****** Object:  Table [dbo].[t_mch_info]    Script Date: 05/07/2020 21:52:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[t_mch_info](
	[mch_id] [varchar](30) NOT NULL,
	[name] [varchar](30) NOT NULL,
	[type] [varchar](24) NOT NULL,
	[req_key] [varchar](128) NOT NULL,
	[res_key] [varchar](128) NOT NULL,
	[status] [int] NOT NULL,
	[create_by] [varchar](64) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](64) NULL,
	[update_time] [datetime] NULL,
	[remark] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[mch_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商户ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_mch_info', @level2type=N'COLUMN',@level2name=N'mch_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商户名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_mch_info', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商户类型 1、平台商户 2、私有商户' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_mch_info', @level2type=N'COLUMN',@level2name=N'type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'请求私钥' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_mch_info', @level2type=N'COLUMN',@level2name=N'req_key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'响应私钥' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_mch_info', @level2type=N'COLUMN',@level2name=N'res_key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_mch_info', @level2type=N'COLUMN',@level2name=N'status'
GO
/****** Object:  Table [dbo].[sys_user_role]    Script Date: 05/07/2020 21:52:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_user_role](
	[user_id] [int] NOT NULL,
	[role_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[user_id] ASC,
	[role_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sys_user_post]    Script Date: 05/07/2020 21:52:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_user_post](
	[user_id] [int] NOT NULL,
	[post_id] [int] NOT NULL,
 CONSTRAINT [PK__sys_user__CA534F796C190EBB] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC,
	[post_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sys_user]    Script Date: 05/07/2020 21:52:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_user](
	[user_id] [int] IDENTITY(1,1) NOT NULL,
	[dept_id] [int] NULL,
	[user_name] [varchar](30) NOT NULL,
	[nick_name] [varchar](30) NOT NULL,
	[user_type] [varchar](2) NULL,
	[email] [varchar](50) NULL,
	[phonenumber] [varchar](11) NULL,
	[sex] [char](1) NULL,
	[avatar] [varchar](100) NULL,
	[password] [varchar](100) NULL,
	[status] [char](1) NULL,
	[del_flag] [char](1) NULL,
	[login_ip] [varchar](50) NULL,
	[login_date] [datetime] NULL,
	[create_by] [varchar](64) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](64) NULL,
	[update_time] [datetime] NULL,
	[remark] [varchar](500) NULL,
 CONSTRAINT [PK_sys_user] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'user_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'dept_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户账号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'user_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户昵称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'nick_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户类型（00系统用户）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'user_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手机号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'phonenumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户性别（0男 1女 2未知）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'sex'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'头像地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'avatar'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'密码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'password'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'帐号状态（0正常 1停用）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'删除标志（0代表存在 2代表删除）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'del_flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最后登陆IP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'login_ip'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最后登陆时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'login_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'create_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'create_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'update_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'update_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'remark'
GO
/****** Object:  Table [dbo].[sys_role_menu]    Script Date: 05/07/2020 21:52:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_role_menu](
	[role_id] [int] NOT NULL,
	[menu_id] [int] NOT NULL,
 CONSTRAINT [PK__sys_role__A2C36A616477ECF3] PRIMARY KEY CLUSTERED 
(
	[role_id] ASC,
	[menu_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sys_role_dept]    Script Date: 05/07/2020 21:52:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_role_dept](
	[role_id] [int] NOT NULL,
	[dept_id] [int] NOT NULL,
 CONSTRAINT [PK__sys_role__2BC3005B68487DD7] PRIMARY KEY CLUSTERED 
(
	[role_id] ASC,
	[dept_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sys_role]    Script Date: 05/07/2020 21:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_role](
	[role_id] [int] IDENTITY(1,1) NOT NULL,
	[role_name] [varchar](30) NOT NULL,
	[role_key] [varchar](100) NOT NULL,
	[role_sort] [int] NOT NULL,
	[data_scope] [char](1) NULL,
	[status] [char](1) NOT NULL,
	[del_flag] [char](1) NULL,
	[create_by] [varchar](64) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](64) NULL,
	[update_time] [datetime] NULL,
	[remark] [varchar](500) NULL,
 CONSTRAINT [PK__sys_role__760965CC48CFD27E] PRIMARY KEY CLUSTERED 
(
	[role_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sys_post]    Script Date: 05/07/2020 21:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_post](
	[post_id] [int] IDENTITY(1,1) NOT NULL,
	[post_code] [varchar](64) NOT NULL,
	[post_name] [varchar](50) NOT NULL,
	[post_sort] [int] NOT NULL,
	[status] [char](1) NOT NULL,
	[create_by] [varchar](64) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](64) NULL,
	[update_time] [datetime] NULL,
	[remark] [varchar](500) NULL,
 CONSTRAINT [PK__sys_post__3ED787664222D4EF] PRIMARY KEY CLUSTERED 
(
	[post_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sys_oper_log]    Script Date: 05/07/2020 21:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_oper_log](
	[oper_id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](20) NULL,
	[business_type] [int] NULL,
	[method] [varchar](500) NULL,
	[request_method] [varchar](10) NULL,
	[operator_type] [int] NULL,
	[oper_name] [varchar](50) NULL,
	[dept_name] [varchar](50) NULL,
	[oper_url] [varchar](500) NULL,
	[oper_ip] [varchar](50) NULL,
	[oper_location] [varchar](500) NULL,
	[oper_param] [varchar](4000) NULL,
	[json_result] [varchar](4000) NULL,
	[status] [int] NULL,
	[error_msg] [varchar](4000) NULL,
	[oper_time] [datetime] NULL,
 CONSTRAINT [PK__sys_oper__34723BF96FE99F9F] PRIMARY KEY CLUSTERED 
(
	[oper_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sys_notice]    Script Date: 05/07/2020 21:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_notice](
	[notice_id] [int] IDENTITY(1,1) NOT NULL,
	[notice_title] [varchar](50) NOT NULL,
	[notice_type] [char](1) NOT NULL,
	[notice_content] [varchar](2000) NULL,
	[status] [char](1) NULL,
	[create_by] [varchar](64) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](64) NULL,
	[update_time] [datetime] NULL,
	[remark] [varchar](255) NULL,
 CONSTRAINT [PK__sys_noti__3E82A5DB42E1EEFE] PRIMARY KEY CLUSTERED 
(
	[notice_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sys_menu]    Script Date: 05/07/2020 21:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_menu](
	[menu_id] [int] IDENTITY(1,1) NOT NULL,
	[menu_name] [varchar](50) NOT NULL,
	[parent_id] [int] NULL,
	[order_num] [int] NULL,
	[path] [varchar](200) NULL,
	[component] [varchar](255) NULL,
	[is_frame] [int] NULL,
	[menu_type] [char](1) NULL,
	[visible] [char](1) NULL,
	[perms] [varchar](100) NULL,
	[icon] [varchar](100) NULL,
	[create_by] [varchar](64) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](64) NULL,
	[update_time] [datetime] NULL,
	[remark] [varchar](500) NULL,
 CONSTRAINT [PK__sys_menu__4CA0FADC5165187F] PRIMARY KEY CLUSTERED 
(
	[menu_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sys_logininfor]    Script Date: 05/07/2020 21:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_logininfor](
	[info_id] [int] IDENTITY(1,1) NOT NULL,
	[user_name] [varchar](50) NULL,
	[ipaddr] [varchar](50) NULL,
	[login_location] [varchar](255) NULL,
	[browser] [varchar](50) NULL,
	[os] [varchar](50) NULL,
	[status] [char](1) NULL,
	[msg] [varchar](255) NULL,
	[login_time] [datetime] NULL,
 CONSTRAINT [PK__sys_logi__3D8A9C1A2645B050] PRIMARY KEY CLUSTERED 
(
	[info_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sys_job_log]    Script Date: 05/07/2020 21:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_job_log](
	[job_log_id] [int] IDENTITY(1,1) NOT NULL,
	[job_name] [varchar](64) NOT NULL,
	[job_group] [varchar](64) NOT NULL,
	[invoke_target] [varchar](500) NOT NULL,
	[job_message] [varchar](500) NULL,
	[status] [char](1) NULL,
	[exception_info] [varchar](2000) NULL,
	[create_time] [datetime] NULL,
 CONSTRAINT [PK__sys_job___0CA0EBC93D2915A8] PRIMARY KEY CLUSTERED 
(
	[job_log_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sys_job]    Script Date: 05/07/2020 21:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_job](
	[job_id] [int] IDENTITY(1,1) NOT NULL,
	[job_name] [varchar](64) NOT NULL,
	[job_group] [varchar](64) NOT NULL,
	[invoke_target] [varchar](500) NOT NULL,
	[cron_expression] [varchar](255) NULL,
	[misfire_policy] [varchar](20) NULL,
	[concurrent] [char](1) NULL,
	[status] [char](1) NULL,
	[create_by] [varchar](64) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](64) NULL,
	[update_time] [datetime] NULL,
	[remark] [varchar](500) NULL,
 CONSTRAINT [PK__sys_job__2E0E56DC30C33EC3] PRIMARY KEY CLUSTERED 
(
	[job_id] ASC,
	[job_name] ASC,
	[job_group] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sys_dict_type]    Script Date: 05/07/2020 21:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_dict_type](
	[dict_id] [int] IDENTITY(1,1) NOT NULL,
	[dict_name] [varchar](100) NULL,
	[dict_type] [varchar](100) NULL,
	[status] [char](1) NULL,
	[create_by] [varchar](64) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](64) NULL,
	[update_time] [datetime] NULL,
	[remark] [varchar](500) NULL,
 CONSTRAINT [PK__sys_dict__3BD4186C01142BA1] PRIMARY KEY CLUSTERED 
(
	[dict_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ__sys_dict__75FC1B9303F0984C] UNIQUE NONCLUSTERED 
(
	[dict_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sys_dict_data]    Script Date: 05/07/2020 21:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_dict_data](
	[dict_code] [int] IDENTITY(1,1) NOT NULL,
	[dict_sort] [int] NULL,
	[dict_label] [varchar](100) NULL,
	[dict_value] [varchar](100) NULL,
	[dict_type] [varchar](100) NULL,
	[css_class] [varchar](100) NULL,
	[list_class] [varchar](100) NULL,
	[is_default] [char](1) NULL,
	[status] [char](1) NULL,
	[create_by] [varchar](64) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](64) NULL,
	[update_time] [datetime] NULL,
	[remark] [varchar](500) NULL,
 CONSTRAINT [PK__sys_dict__19CBC34B0D7A0286] PRIMARY KEY CLUSTERED 
(
	[dict_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sys_dept]    Script Date: 05/07/2020 21:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_dept](
	[dept_id] [int] IDENTITY(1,1) NOT NULL,
	[parent_id] [int] NULL,
	[ancestors] [varchar](50) NULL,
	[dept_name] [varchar](30) NULL,
	[order_num] [int] NULL,
	[leader] [varchar](20) NULL,
	[phone] [varchar](11) NULL,
	[email] [varchar](50) NULL,
	[status] [char](1) NULL,
	[del_flag] [char](1) NULL,
	[create_by] [varchar](64) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](64) NULL,
	[update_time] [datetime] NULL,
	[remark] [varchar](500) NULL,
 CONSTRAINT [PK__sys_dept__DCA659747F60ED59] PRIMARY KEY CLUSTERED 
(
	[dept_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dept', @level2type=N'COLUMN',@level2name=N'dept_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'父部门id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dept', @level2type=N'COLUMN',@level2name=N'parent_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'祖级列表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dept', @level2type=N'COLUMN',@level2name=N'ancestors'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dept', @level2type=N'COLUMN',@level2name=N'dept_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'显示顺序' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dept', @level2type=N'COLUMN',@level2name=N'order_num'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'负责人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dept', @level2type=N'COLUMN',@level2name=N'leader'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dept', @level2type=N'COLUMN',@level2name=N'phone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dept', @level2type=N'COLUMN',@level2name=N'email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门状态（0正常 1停用）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dept', @level2type=N'COLUMN',@level2name=N'status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'删除标志（0代表存在 2代表删除）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dept', @level2type=N'COLUMN',@level2name=N'del_flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dept', @level2type=N'COLUMN',@level2name=N'create_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dept', @level2type=N'COLUMN',@level2name=N'create_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dept', @level2type=N'COLUMN',@level2name=N'update_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dept', @level2type=N'COLUMN',@level2name=N'update_time'
GO
/****** Object:  Table [dbo].[sys_config]    Script Date: 05/07/2020 21:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_config](
	[config_id] [int] IDENTITY(1,1) NOT NULL,
	[config_name] [varchar](100) NULL,
	[config_key] [varchar](100) NULL,
	[config_value] [varchar](500) NULL,
	[config_type] [char](1) NULL,
	[create_by] [varchar](64) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](64) NULL,
	[update_time] [datetime] NULL,
	[remark] [varchar](500) NULL,
 CONSTRAINT [PK__sys_conf__4AD1BFF11BC821DD] PRIMARY KEY CLUSTERED 
(
	[config_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gen_table_column]    Script Date: 05/07/2020 21:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gen_table_column](
	[column_id] [int] IDENTITY(1,1) NOT NULL,
	[table_id] [varchar](64) NULL,
	[column_name] [varchar](200) NULL,
	[column_comment] [varchar](500) NULL,
	[column_type] [varchar](100) NULL,
	[java_type] [varchar](500) NULL,
	[java_field] [varchar](200) NULL,
	[is_pk] [char](1) NULL,
	[is_increment] [char](1) NULL,
	[is_required] [char](1) NULL,
	[is_insert] [char](1) NULL,
	[is_edit] [char](1) NULL,
	[is_list] [char](1) NULL,
	[is_query] [char](1) NULL,
	[query_type] [varchar](200) NULL,
	[html_type] [varchar](200) NULL,
	[dict_type] [varchar](200) NULL,
	[sort] [int] NULL,
	[create_by] [varchar](64) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](64) NULL,
	[update_time] [datetime] NULL,
 CONSTRAINT [PK__gen_tabl__E301851F55F4C372] PRIMARY KEY CLUSTERED 
(
	[column_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gen_table]    Script Date: 05/07/2020 21:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gen_table](
	[table_id] [int] IDENTITY(1,1) NOT NULL,
	[table_name] [varchar](200) NULL,
	[table_comment] [varchar](500) NULL,
	[class_name] [varchar](100) NULL,
	[tpl_category] [varchar](200) NULL,
	[package_name] [varchar](100) NULL,
	[module_name] [varchar](30) NULL,
	[business_name] [varchar](30) NULL,
	[function_name] [varchar](50) NULL,
	[function_author] [varchar](50) NULL,
	[options] [varchar](1000) NULL,
	[create_by] [varchar](64) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](64) NULL,
	[update_time] [datetime] NULL,
	[remark] [varchar](500) NULL,
 CONSTRAINT [PK__gen_tabl__B21E8F244B7734FF] PRIMARY KEY CLUSTERED 
(
	[table_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Default [DF__gen_table__table__4D5F7D71]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[gen_table] ADD  CONSTRAINT [DF__gen_table__table__4D5F7D71]  DEFAULT ('') FOR [table_name]
GO
/****** Object:  Default [DF__gen_table__table__4E53A1AA]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[gen_table] ADD  CONSTRAINT [DF__gen_table__table__4E53A1AA]  DEFAULT ('') FOR [table_comment]
GO
/****** Object:  Default [DF__gen_table__class__4F47C5E3]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[gen_table] ADD  CONSTRAINT [DF__gen_table__class__4F47C5E3]  DEFAULT ('') FOR [class_name]
GO
/****** Object:  Default [DF__gen_table__tpl_c__503BEA1C]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[gen_table] ADD  CONSTRAINT [DF__gen_table__tpl_c__503BEA1C]  DEFAULT ('crud') FOR [tpl_category]
GO
/****** Object:  Default [DF__gen_table__creat__51300E55]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[gen_table] ADD  CONSTRAINT [DF__gen_table__creat__51300E55]  DEFAULT ('') FOR [create_by]
GO
/****** Object:  Default [DF__gen_table__updat__5224328E]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[gen_table] ADD  CONSTRAINT [DF__gen_table__updat__5224328E]  DEFAULT ('') FOR [update_by]
GO
/****** Object:  Default [DF__gen_table__remar__531856C7]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[gen_table] ADD  CONSTRAINT [DF__gen_table__remar__531856C7]  DEFAULT (NULL) FOR [remark]
GO
/****** Object:  Default [DF__gen_table__query__57DD0BE4]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[gen_table_column] ADD  CONSTRAINT [DF__gen_table__query__57DD0BE4]  DEFAULT ('EQ') FOR [query_type]
GO
/****** Object:  Default [DF__gen_table__dict___58D1301D]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[gen_table_column] ADD  CONSTRAINT [DF__gen_table__dict___58D1301D]  DEFAULT ('') FOR [dict_type]
GO
/****** Object:  Default [DF__gen_table__creat__59C55456]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[gen_table_column] ADD  CONSTRAINT [DF__gen_table__creat__59C55456]  DEFAULT ('') FOR [create_by]
GO
/****** Object:  Default [DF__gen_table__updat__5AB9788F]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[gen_table_column] ADD  CONSTRAINT [DF__gen_table__updat__5AB9788F]  DEFAULT ('') FOR [update_by]
GO
/****** Object:  Default [DF__sys_confi__confi__1DB06A4F]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_config] ADD  CONSTRAINT [DF__sys_confi__confi__1DB06A4F]  DEFAULT ('') FOR [config_name]
GO
/****** Object:  Default [DF__sys_confi__confi__1EA48E88]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_config] ADD  CONSTRAINT [DF__sys_confi__confi__1EA48E88]  DEFAULT ('') FOR [config_key]
GO
/****** Object:  Default [DF__sys_confi__confi__1F98B2C1]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_config] ADD  CONSTRAINT [DF__sys_confi__confi__1F98B2C1]  DEFAULT ('') FOR [config_value]
GO
/****** Object:  Default [DF__sys_confi__confi__208CD6FA]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_config] ADD  CONSTRAINT [DF__sys_confi__confi__208CD6FA]  DEFAULT ('N') FOR [config_type]
GO
/****** Object:  Default [DF__sys_confi__creat__2180FB33]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_config] ADD  CONSTRAINT [DF__sys_confi__creat__2180FB33]  DEFAULT ('') FOR [create_by]
GO
/****** Object:  Default [DF__sys_confi__updat__22751F6C]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_config] ADD  CONSTRAINT [DF__sys_confi__updat__22751F6C]  DEFAULT ('') FOR [update_by]
GO
/****** Object:  Default [DF__sys_confi__remar__236943A5]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_config] ADD  CONSTRAINT [DF__sys_confi__remar__236943A5]  DEFAULT (NULL) FOR [remark]
GO
/****** Object:  Default [DF__sys_dept__parent__014935CB]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dept] ADD  CONSTRAINT [DF__sys_dept__parent__014935CB]  DEFAULT ((0)) FOR [parent_id]
GO
/****** Object:  Default [DF__sys_dept__ancest__023D5A04]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dept] ADD  CONSTRAINT [DF__sys_dept__ancest__023D5A04]  DEFAULT ('') FOR [ancestors]
GO
/****** Object:  Default [DF__sys_dept__dept_n__03317E3D]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dept] ADD  CONSTRAINT [DF__sys_dept__dept_n__03317E3D]  DEFAULT ('') FOR [dept_name]
GO
/****** Object:  Default [DF__sys_dept__order___0425A276]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dept] ADD  CONSTRAINT [DF__sys_dept__order___0425A276]  DEFAULT ((0)) FOR [order_num]
GO
/****** Object:  Default [DF__sys_dept__leader__0519C6AF]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dept] ADD  CONSTRAINT [DF__sys_dept__leader__0519C6AF]  DEFAULT (NULL) FOR [leader]
GO
/****** Object:  Default [DF__sys_dept__phone__060DEAE8]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dept] ADD  CONSTRAINT [DF__sys_dept__phone__060DEAE8]  DEFAULT (NULL) FOR [phone]
GO
/****** Object:  Default [DF__sys_dept__email__07020F21]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dept] ADD  CONSTRAINT [DF__sys_dept__email__07020F21]  DEFAULT (NULL) FOR [email]
GO
/****** Object:  Default [DF__sys_dept__status__07F6335A]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dept] ADD  CONSTRAINT [DF__sys_dept__status__07F6335A]  DEFAULT ('0') FOR [status]
GO
/****** Object:  Default [DF__sys_dept__del_fl__08EA5793]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dept] ADD  CONSTRAINT [DF__sys_dept__del_fl__08EA5793]  DEFAULT ('0') FOR [del_flag]
GO
/****** Object:  Default [DF__sys_dept__create__09DE7BCC]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dept] ADD  CONSTRAINT [DF__sys_dept__create__09DE7BCC]  DEFAULT ('') FOR [create_by]
GO
/****** Object:  Default [DF__sys_dept__update__0AD2A005]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dept] ADD  CONSTRAINT [DF__sys_dept__update__0AD2A005]  DEFAULT ('') FOR [update_by]
GO
/****** Object:  Default [DF__sys_dict___dict___0F624AF8]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dict_data] ADD  CONSTRAINT [DF__sys_dict___dict___0F624AF8]  DEFAULT ((0)) FOR [dict_sort]
GO
/****** Object:  Default [DF__sys_dict___dict___10566F31]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dict_data] ADD  CONSTRAINT [DF__sys_dict___dict___10566F31]  DEFAULT ('') FOR [dict_label]
GO
/****** Object:  Default [DF__sys_dict___dict___114A936A]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dict_data] ADD  CONSTRAINT [DF__sys_dict___dict___114A936A]  DEFAULT ('') FOR [dict_value]
GO
/****** Object:  Default [DF__sys_dict___dict___123EB7A3]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dict_data] ADD  CONSTRAINT [DF__sys_dict___dict___123EB7A3]  DEFAULT ('') FOR [dict_type]
GO
/****** Object:  Default [DF__sys_dict___css_c__1332DBDC]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dict_data] ADD  CONSTRAINT [DF__sys_dict___css_c__1332DBDC]  DEFAULT (NULL) FOR [css_class]
GO
/****** Object:  Default [DF__sys_dict___list___14270015]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dict_data] ADD  CONSTRAINT [DF__sys_dict___list___14270015]  DEFAULT (NULL) FOR [list_class]
GO
/****** Object:  Default [DF__sys_dict___is_de__151B244E]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dict_data] ADD  CONSTRAINT [DF__sys_dict___is_de__151B244E]  DEFAULT ('N') FOR [is_default]
GO
/****** Object:  Default [DF__sys_dict___statu__160F4887]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dict_data] ADD  CONSTRAINT [DF__sys_dict___statu__160F4887]  DEFAULT ('0') FOR [status]
GO
/****** Object:  Default [DF__sys_dict___creat__17036CC0]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dict_data] ADD  CONSTRAINT [DF__sys_dict___creat__17036CC0]  DEFAULT ('') FOR [create_by]
GO
/****** Object:  Default [DF__sys_dict___updat__17F790F9]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dict_data] ADD  CONSTRAINT [DF__sys_dict___updat__17F790F9]  DEFAULT ('') FOR [update_by]
GO
/****** Object:  Default [DF__sys_dict___remar__18EBB532]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dict_data] ADD  CONSTRAINT [DF__sys_dict___remar__18EBB532]  DEFAULT (NULL) FOR [remark]
GO
/****** Object:  Default [DF__sys_dict___dict___05D8E0BE]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dict_type] ADD  CONSTRAINT [DF__sys_dict___dict___05D8E0BE]  DEFAULT ('') FOR [dict_name]
GO
/****** Object:  Default [DF__sys_dict___dict___06CD04F7]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dict_type] ADD  CONSTRAINT [DF__sys_dict___dict___06CD04F7]  DEFAULT ('') FOR [dict_type]
GO
/****** Object:  Default [DF__sys_dict___statu__07C12930]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dict_type] ADD  CONSTRAINT [DF__sys_dict___statu__07C12930]  DEFAULT ('0') FOR [status]
GO
/****** Object:  Default [DF__sys_dict___creat__08B54D69]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dict_type] ADD  CONSTRAINT [DF__sys_dict___creat__08B54D69]  DEFAULT ('') FOR [create_by]
GO
/****** Object:  Default [DF__sys_dict___updat__09A971A2]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dict_type] ADD  CONSTRAINT [DF__sys_dict___updat__09A971A2]  DEFAULT ('') FOR [update_by]
GO
/****** Object:  Default [DF__sys_dict___remar__0A9D95DB]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_dict_type] ADD  CONSTRAINT [DF__sys_dict___remar__0A9D95DB]  DEFAULT (NULL) FOR [remark]
GO
/****** Object:  Default [DF__sys_job__job_nam__32AB8735]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_job] ADD  CONSTRAINT [DF__sys_job__job_nam__32AB8735]  DEFAULT ('') FOR [job_name]
GO
/****** Object:  Default [DF__sys_job__job_gro__339FAB6E]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_job] ADD  CONSTRAINT [DF__sys_job__job_gro__339FAB6E]  DEFAULT ('DEFAULT') FOR [job_group]
GO
/****** Object:  Default [DF__sys_job__cron_ex__3493CFA7]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_job] ADD  CONSTRAINT [DF__sys_job__cron_ex__3493CFA7]  DEFAULT ('') FOR [cron_expression]
GO
/****** Object:  Default [DF__sys_job__misfire__3587F3E0]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_job] ADD  CONSTRAINT [DF__sys_job__misfire__3587F3E0]  DEFAULT ('3') FOR [misfire_policy]
GO
/****** Object:  Default [DF__sys_job__concurr__367C1819]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_job] ADD  CONSTRAINT [DF__sys_job__concurr__367C1819]  DEFAULT ('1') FOR [concurrent]
GO
/****** Object:  Default [DF__sys_job__status__37703C52]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_job] ADD  CONSTRAINT [DF__sys_job__status__37703C52]  DEFAULT ('0') FOR [status]
GO
/****** Object:  Default [DF__sys_job__create___3864608B]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_job] ADD  CONSTRAINT [DF__sys_job__create___3864608B]  DEFAULT ('') FOR [create_by]
GO
/****** Object:  Default [DF__sys_job__update___395884C4]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_job] ADD  CONSTRAINT [DF__sys_job__update___395884C4]  DEFAULT ('') FOR [update_by]
GO
/****** Object:  Default [DF__sys_job__remark__3A4CA8FD]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_job] ADD  CONSTRAINT [DF__sys_job__remark__3A4CA8FD]  DEFAULT ('') FOR [remark]
GO
/****** Object:  Default [DF__sys_job_l__statu__3F115E1A]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_job_log] ADD  CONSTRAINT [DF__sys_job_l__statu__3F115E1A]  DEFAULT ('0') FOR [status]
GO
/****** Object:  Default [DF__sys_job_l__excep__40058253]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_job_log] ADD  CONSTRAINT [DF__sys_job_l__excep__40058253]  DEFAULT ('') FOR [exception_info]
GO
/****** Object:  Default [DF__sys_login__user___282DF8C2]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_logininfor] ADD  CONSTRAINT [DF__sys_login__user___282DF8C2]  DEFAULT ('') FOR [user_name]
GO
/****** Object:  Default [DF__sys_login__ipadd__29221CFB]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_logininfor] ADD  CONSTRAINT [DF__sys_login__ipadd__29221CFB]  DEFAULT ('') FOR [ipaddr]
GO
/****** Object:  Default [DF__sys_login__login__2A164134]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_logininfor] ADD  CONSTRAINT [DF__sys_login__login__2A164134]  DEFAULT ('') FOR [login_location]
GO
/****** Object:  Default [DF__sys_login__brows__2B0A656D]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_logininfor] ADD  CONSTRAINT [DF__sys_login__brows__2B0A656D]  DEFAULT ('') FOR [browser]
GO
/****** Object:  Default [DF__sys_logininf__os__2BFE89A6]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_logininfor] ADD  CONSTRAINT [DF__sys_logininf__os__2BFE89A6]  DEFAULT ('') FOR [os]
GO
/****** Object:  Default [DF__sys_login__statu__2CF2ADDF]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_logininfor] ADD  CONSTRAINT [DF__sys_login__statu__2CF2ADDF]  DEFAULT ('0') FOR [status]
GO
/****** Object:  Default [DF__sys_loginin__msg__2DE6D218]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_logininfor] ADD  CONSTRAINT [DF__sys_loginin__msg__2DE6D218]  DEFAULT ('') FOR [msg]
GO
/****** Object:  Default [DF__sys_menu__parent__534D60F1]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_menu] ADD  CONSTRAINT [DF__sys_menu__parent__534D60F1]  DEFAULT ((0)) FOR [parent_id]
GO
/****** Object:  Default [DF__sys_menu__order___5441852A]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_menu] ADD  CONSTRAINT [DF__sys_menu__order___5441852A]  DEFAULT ((0)) FOR [order_num]
GO
/****** Object:  Default [DF__sys_menu__path__5535A963]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_menu] ADD  CONSTRAINT [DF__sys_menu__path__5535A963]  DEFAULT ('') FOR [path]
GO
/****** Object:  Default [DF__sys_menu__compon__5629CD9C]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_menu] ADD  CONSTRAINT [DF__sys_menu__compon__5629CD9C]  DEFAULT (NULL) FOR [component]
GO
/****** Object:  Default [DF__sys_menu__is_fra__571DF1D5]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_menu] ADD  CONSTRAINT [DF__sys_menu__is_fra__571DF1D5]  DEFAULT ((1)) FOR [is_frame]
GO
/****** Object:  Default [DF__sys_menu__menu_t__5812160E]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_menu] ADD  CONSTRAINT [DF__sys_menu__menu_t__5812160E]  DEFAULT ('') FOR [menu_type]
GO
/****** Object:  Default [DF__sys_menu__visibl__59063A47]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_menu] ADD  CONSTRAINT [DF__sys_menu__visibl__59063A47]  DEFAULT ((0)) FOR [visible]
GO
/****** Object:  Default [DF__sys_menu__perms__59FA5E80]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_menu] ADD  CONSTRAINT [DF__sys_menu__perms__59FA5E80]  DEFAULT (NULL) FOR [perms]
GO
/****** Object:  Default [DF__sys_menu__icon__5AEE82B9]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_menu] ADD  CONSTRAINT [DF__sys_menu__icon__5AEE82B9]  DEFAULT ('#') FOR [icon]
GO
/****** Object:  Default [DF__sys_menu__create__5BE2A6F2]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_menu] ADD  CONSTRAINT [DF__sys_menu__create__5BE2A6F2]  DEFAULT ('') FOR [create_by]
GO
/****** Object:  Default [DF__sys_menu__update__5CD6CB2B]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_menu] ADD  CONSTRAINT [DF__sys_menu__update__5CD6CB2B]  DEFAULT ('') FOR [update_by]
GO
/****** Object:  Default [DF__sys_menu__remark__5DCAEF64]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_menu] ADD  CONSTRAINT [DF__sys_menu__remark__5DCAEF64]  DEFAULT ('') FOR [remark]
GO
/****** Object:  Default [DF__sys_notic__notic__44CA3770]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_notice] ADD  CONSTRAINT [DF__sys_notic__notic__44CA3770]  DEFAULT (NULL) FOR [notice_content]
GO
/****** Object:  Default [DF__sys_notic__statu__45BE5BA9]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_notice] ADD  CONSTRAINT [DF__sys_notic__statu__45BE5BA9]  DEFAULT ('0') FOR [status]
GO
/****** Object:  Default [DF__sys_notic__creat__46B27FE2]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_notice] ADD  CONSTRAINT [DF__sys_notic__creat__46B27FE2]  DEFAULT ('') FOR [create_by]
GO
/****** Object:  Default [DF__sys_notic__updat__47A6A41B]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_notice] ADD  CONSTRAINT [DF__sys_notic__updat__47A6A41B]  DEFAULT ('') FOR [update_by]
GO
/****** Object:  Default [DF__sys_notic__remar__489AC854]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_notice] ADD  CONSTRAINT [DF__sys_notic__remar__489AC854]  DEFAULT (NULL) FOR [remark]
GO
/****** Object:  Default [DF__sys_oper___title__71D1E811]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_oper_log] ADD  CONSTRAINT [DF__sys_oper___title__71D1E811]  DEFAULT ('') FOR [title]
GO
/****** Object:  Default [DF__sys_oper___busin__72C60C4A]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_oper_log] ADD  CONSTRAINT [DF__sys_oper___busin__72C60C4A]  DEFAULT ((0)) FOR [business_type]
GO
/****** Object:  Default [DF__sys_oper___metho__73BA3083]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_oper_log] ADD  CONSTRAINT [DF__sys_oper___metho__73BA3083]  DEFAULT ('') FOR [method]
GO
/****** Object:  Default [DF__sys_oper___reque__74AE54BC]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_oper_log] ADD  CONSTRAINT [DF__sys_oper___reque__74AE54BC]  DEFAULT ('') FOR [request_method]
GO
/****** Object:  Default [DF__sys_oper___opera__75A278F5]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_oper_log] ADD  CONSTRAINT [DF__sys_oper___opera__75A278F5]  DEFAULT ((0)) FOR [operator_type]
GO
/****** Object:  Default [DF__sys_oper___oper___76969D2E]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_oper_log] ADD  CONSTRAINT [DF__sys_oper___oper___76969D2E]  DEFAULT ('') FOR [oper_name]
GO
/****** Object:  Default [DF__sys_oper___dept___778AC167]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_oper_log] ADD  CONSTRAINT [DF__sys_oper___dept___778AC167]  DEFAULT ('') FOR [dept_name]
GO
/****** Object:  Default [DF__sys_oper___oper___787EE5A0]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_oper_log] ADD  CONSTRAINT [DF__sys_oper___oper___787EE5A0]  DEFAULT ('') FOR [oper_url]
GO
/****** Object:  Default [DF__sys_oper___oper___797309D9]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_oper_log] ADD  CONSTRAINT [DF__sys_oper___oper___797309D9]  DEFAULT ('') FOR [oper_ip]
GO
/****** Object:  Default [DF__sys_oper___oper___7A672E12]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_oper_log] ADD  CONSTRAINT [DF__sys_oper___oper___7A672E12]  DEFAULT ('') FOR [oper_location]
GO
/****** Object:  Default [DF__sys_oper___oper___7B5B524B]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_oper_log] ADD  CONSTRAINT [DF__sys_oper___oper___7B5B524B]  DEFAULT ('') FOR [oper_param]
GO
/****** Object:  Default [DF__sys_oper___json___7C4F7684]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_oper_log] ADD  CONSTRAINT [DF__sys_oper___json___7C4F7684]  DEFAULT ('') FOR [json_result]
GO
/****** Object:  Default [DF__sys_oper___statu__7D439ABD]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_oper_log] ADD  CONSTRAINT [DF__sys_oper___statu__7D439ABD]  DEFAULT ((0)) FOR [status]
GO
/****** Object:  Default [DF__sys_oper___error__7E37BEF6]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_oper_log] ADD  CONSTRAINT [DF__sys_oper___error__7E37BEF6]  DEFAULT ('') FOR [error_msg]
GO
/****** Object:  Default [DF__sys_post__create__440B1D61]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_post] ADD  CONSTRAINT [DF__sys_post__create__440B1D61]  DEFAULT ('') FOR [create_by]
GO
/****** Object:  Default [DF__sys_post__update__44FF419A]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_post] ADD  CONSTRAINT [DF__sys_post__update__44FF419A]  DEFAULT ('') FOR [update_by]
GO
/****** Object:  Default [DF__sys_post__remark__45F365D3]    Script Date: 05/07/2020 21:52:24 ******/
ALTER TABLE [dbo].[sys_post] ADD  CONSTRAINT [DF__sys_post__remark__45F365D3]  DEFAULT (NULL) FOR [remark]
GO
/****** Object:  Default [DF__sys_role__data_s__4AB81AF0]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[sys_role] ADD  CONSTRAINT [DF__sys_role__data_s__4AB81AF0]  DEFAULT ('1') FOR [data_scope]
GO
/****** Object:  Default [DF__sys_role__del_fl__4BAC3F29]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[sys_role] ADD  CONSTRAINT [DF__sys_role__del_fl__4BAC3F29]  DEFAULT ('0') FOR [del_flag]
GO
/****** Object:  Default [DF__sys_role__create__4CA06362]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[sys_role] ADD  CONSTRAINT [DF__sys_role__create__4CA06362]  DEFAULT ('') FOR [create_by]
GO
/****** Object:  Default [DF__sys_role__update__4D94879B]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[sys_role] ADD  CONSTRAINT [DF__sys_role__update__4D94879B]  DEFAULT ('') FOR [update_by]
GO
/****** Object:  Default [DF__sys_role__remark__4E88ABD4]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[sys_role] ADD  CONSTRAINT [DF__sys_role__remark__4E88ABD4]  DEFAULT (NULL) FOR [remark]
GO
/****** Object:  Default [DF__sys_user__dept_i__25869641]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[sys_user] ADD  CONSTRAINT [DF__sys_user__dept_i__25869641]  DEFAULT (NULL) FOR [dept_id]
GO
/****** Object:  Default [DF__sys_user__user_t__267ABA7A]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[sys_user] ADD  CONSTRAINT [DF__sys_user__user_t__267ABA7A]  DEFAULT ('00') FOR [user_type]
GO
/****** Object:  Default [DF__sys_user__email__276EDEB3]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[sys_user] ADD  CONSTRAINT [DF__sys_user__email__276EDEB3]  DEFAULT ('') FOR [email]
GO
/****** Object:  Default [DF__sys_user__phonen__286302EC]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[sys_user] ADD  CONSTRAINT [DF__sys_user__phonen__286302EC]  DEFAULT ('') FOR [phonenumber]
GO
/****** Object:  Default [DF__sys_user__sex__29572725]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[sys_user] ADD  CONSTRAINT [DF__sys_user__sex__29572725]  DEFAULT ('0') FOR [sex]
GO
/****** Object:  Default [DF__sys_user__avatar__2A4B4B5E]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[sys_user] ADD  CONSTRAINT [DF__sys_user__avatar__2A4B4B5E]  DEFAULT ('') FOR [avatar]
GO
/****** Object:  Default [DF__sys_user__passwo__2B3F6F97]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[sys_user] ADD  CONSTRAINT [DF__sys_user__passwo__2B3F6F97]  DEFAULT ('') FOR [password]
GO
/****** Object:  Default [DF__sys_user__status__2C3393D0]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[sys_user] ADD  CONSTRAINT [DF__sys_user__status__2C3393D0]  DEFAULT ('0') FOR [status]
GO
/****** Object:  Default [DF__sys_user__del_fl__2D27B809]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[sys_user] ADD  CONSTRAINT [DF__sys_user__del_fl__2D27B809]  DEFAULT ('0') FOR [del_flag]
GO
/****** Object:  Default [DF__sys_user__login___2E1BDC42]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[sys_user] ADD  CONSTRAINT [DF__sys_user__login___2E1BDC42]  DEFAULT ('') FOR [login_ip]
GO
/****** Object:  Default [DF__sys_user__create__2F10007B]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[sys_user] ADD  CONSTRAINT [DF__sys_user__create__2F10007B]  DEFAULT ('') FOR [create_by]
GO
/****** Object:  Default [DF__sys_user__update__300424B4]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[sys_user] ADD  CONSTRAINT [DF__sys_user__update__300424B4]  DEFAULT ('') FOR [update_by]
GO
/****** Object:  Default [DF__sys_user__remark__30F848ED]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[sys_user] ADD  CONSTRAINT [DF__sys_user__remark__30F848ED]  DEFAULT (NULL) FOR [remark]
GO
/****** Object:  Default [DF__t_mch_inf__state__5C6CB6D7]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_mch_info] ADD  DEFAULT ('1') FOR [status]
GO
/****** Object:  Default [DF__t_mch_not__Notif__5EBF139D]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_mch_notify] ADD  CONSTRAINT [DF__t_mch_not__Notif__5EBF139D]  DEFAULT ((0)) FOR [notify_count]
GO
/****** Object:  Default [DF__t_mch_not__Resul__5FB337D6]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_mch_notify] ADD  CONSTRAINT [DF__t_mch_not__Resul__5FB337D6]  DEFAULT (NULL) FOR [result]
GO
/****** Object:  Default [DF__t_mch_not__Statu__60A75C0F]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_mch_notify] ADD  CONSTRAINT [DF__t_mch_not__Statu__60A75C0F]  DEFAULT ((1)) FOR [status]
GO
/****** Object:  Default [DF__t_mch_not__LastN__619B8048]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_mch_notify] ADD  CONSTRAINT [DF__t_mch_not__LastN__619B8048]  DEFAULT (NULL) FOR [last_notify_time]
GO
/****** Object:  Default [DF__t_pay_cha__State__07F6335A]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_pay_channel] ADD  CONSTRAINT [DF__t_pay_cha__State__07F6335A]  DEFAULT ('1') FOR [status]
GO
/****** Object:  Default [DF__t_pay_ord__Curre__15502E78]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Curre__15502E78]  DEFAULT ('cny') FOR [currency]
GO
/****** Object:  Default [DF__t_pay_ord__Statu__164452B1]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Statu__164452B1]  DEFAULT ((0)) FOR [status]
GO
/****** Object:  Default [DF__t_pay_ord__Clien__173876EA]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Clien__173876EA]  DEFAULT (NULL) FOR [client_ip]
GO
/****** Object:  Default [DF__t_pay_ord__Devic__182C9B23]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Devic__182C9B23]  DEFAULT (NULL) FOR [device]
GO
/****** Object:  Default [DF__t_pay_ord__Extra__1920BF5C]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Extra__1920BF5C]  DEFAULT (NULL) FOR [extra]
GO
/****** Object:  Default [DF__t_pay_ord__Chann__1A14E395]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Chann__1A14E395]  DEFAULT (NULL) FOR [channel_order_no]
GO
/****** Object:  Default [DF__t_pay_ord__ErrCo__1B0907CE]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__ErrCo__1B0907CE]  DEFAULT (NULL) FOR [err_code]
GO
/****** Object:  Default [DF__t_pay_ord__ErrMs__1BFD2C07]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__ErrMs__1BFD2C07]  DEFAULT (NULL) FOR [err_msg]
GO
/****** Object:  Default [DF__t_pay_ord__Param__1CF15040]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Param__1CF15040]  DEFAULT (NULL) FOR [param1]
GO
/****** Object:  Default [DF__t_pay_ord__Param__1DE57479]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Param__1DE57479]  DEFAULT (NULL) FOR [param2]
GO
/****** Object:  Default [DF__t_pay_ord__Notif__1ED998B2]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Notif__1ED998B2]  DEFAULT ((0)) FOR [notify_count]
GO
/****** Object:  Default [DF__t_pay_ord__LastN__1FCDBCEB]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__LastN__1FCDBCEB]  DEFAULT (NULL) FOR [last_notify_time]
GO
/****** Object:  Default [DF__t_pay_ord__Expir__20C1E124]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Expir__20C1E124]  DEFAULT (NULL) FOR [expire_time]
GO
/****** Object:  Default [DF__t_pay_ord__PaySu__21B6055D]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__PaySu__21B6055D]  DEFAULT (NULL) FOR [pay_succ_time]
GO
/****** Object:  Default [DF__t_refund___chann__7814D14C]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [channel_pay_order_no]
GO
/****** Object:  Default [DF__t_refund___curre__7908F585]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT ('cny') FOR [currency]
GO
/****** Object:  Default [DF__t_refund___statu__79FD19BE]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT ('0') FOR [status]
GO
/****** Object:  Default [DF__t_refund___resul__7AF13DF7]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT ('0') FOR [result]
GO
/****** Object:  Default [DF__t_refund___clien__7BE56230]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [client_ip]
GO
/****** Object:  Default [DF__t_refund___devic__7CD98669]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [device]
GO
/****** Object:  Default [DF__t_refund___remar__7DCDAAA2]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [remark_info]
GO
/****** Object:  Default [DF__t_refund___chann__7EC1CEDB]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [channel_user]
GO
/****** Object:  Default [DF__t_refund___user___7FB5F314]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [user_name]
GO
/****** Object:  Default [DF__t_refund___chann__00AA174D]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [channel_order_no]
GO
/****** Object:  Default [DF__t_refund___chann__019E3B86]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [channel_err_code]
GO
/****** Object:  Default [DF__t_refund___chann__02925FBF]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [channel_err_msg]
GO
/****** Object:  Default [DF__t_refund___extra__038683F8]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [extra]
GO
/****** Object:  Default [DF__t_refund___param__047AA831]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [param1]
GO
/****** Object:  Default [DF__t_refund___param__056ECC6A]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [param2]
GO
/****** Object:  Default [DF__t_refund___expir__0662F0A3]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [expire_time]
GO
/****** Object:  Default [DF__t_refund___refun__075714DC]    Script Date: 05/07/2020 21:52:25 ******/
ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [refund_succ_time]
GO
